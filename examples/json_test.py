#!/usr/bin/env python
"""

Example script to show returning data as json.

"""

import sys
import os

from CondCore.Utilities.CondDBFW import querying_framework_api

class query_script():
	def script(self, connection):
		tags = connection.tag().all(amount=sys.argv[1])
		return tags

secrets_file = os.path.join(os.environ["HOME"], ".cms_cond", "netrc")
if not os.path.exists(secrets_file):
    secrets_file = "/afs/cern.ch/cms/DB/conddb/.cms_cond/netrc"

connection_data = {"db_alias" : "orapro", "schema" : "cms_conditions", "host" : "oracle", "secrets" : secrets_file}
qf = querying_framework_api(connection_data)
data = qf.run_script(query_script()).as_dicts()

import pprint
pprint.pprint(data)