#!/usr/bin/env python
"""

Written by Joshua Dawes - CERN, CMS - The University of Manchester

This mini-framework will provide tools to query with different data sources, and managing conditions data by manipulating objects.

"""

from .querying import connect
from .data_sources import json_data_node, json_list, json_dict, json_basic

class querying_framework_api():
	def __init__(self, connection_data):
		self.connection_data = connection_data

	# this will pass a new connection - not a session, since we should allow creation of new sessions in script
	def run_script(self, script_object, output=True):
		# run script object given - look for script method
		if output:
			print("Connecting to database '%s'..." % self.connection_data["db_alias"])
		connection = connect(self.connection_data)
		if output:
			print("Connected.")
			print("Running your script...")
		data = script_object.script(connection)
		# note session() sets the session property, so the line below works
		connection.close_session()
		#if data.__class__.__name__ in ["json_data_node", "json_list", "json_dict", "json_basic"]:
		#	return data.data()
		#else:
		return data

	# configuration methods for framework here